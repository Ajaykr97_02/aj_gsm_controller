/*
 * sha256.c
 *
 *  Created on: May 12, 2021
 *      Author: AJAY
 */
#include "hashMain.h"

/****************************************************************************************/
uint8_t HK[] = {0xe9, 0x2a, 0x29, 0x1b, 0x2e, 0x8b, 0x05, 0x91, 0xdf, 0x66, 0x4b,
			0x80, 0x42, 0xc1, 0x12, 0x80, 0xa7, 0xb2, 0xde, 0xcb, 0x00, 0x3f, 0xbb,
			0x96, 0x49, 0xdb, 0xb2, 0x3c, 0x09, 0x28, 0x40, 0x01, 0x6e, 0x24, 0x51,
			0x44, 0xd6, 0x95, 0xc6, 0x8b, 0x4e, 0xda, 0x02, 0x3e, 0xf7, 0x4d, 0xd3,
			0xcf, 0x88, 0xe0, 0xbc, 0xa5, 0xe2, 0x1b, 0xa3, 0xb2, 0x99, 0x2c, 0x07,
			0x7a, 0x6e, 0xa7, 0x8c, 0x82};
/****************************************************************************************/
void testSHA()
{
	uint8_t data[] = "hello";
	uint8_t digest[32];
	int32_t digestLength = 32;

	computeHMACsha256Hash(data , 5 , digest , &digestLength ,HK ,sizeof(HK));
}


int32_t computeHMACsha256Hash(uint8_t *inputMessage, uint32_t inputMessageLength,	uint8_t *messageDigest, int32_t *messageDigestLength ,uint8_t *Key , uint16_t keyLen)
{
	HMAC_SHA256ctx_stt HMACSha256Ctx;
	uint32_t errorStatus = HASH_SUCCESS;

	HMACSha256Ctx.mFlags   = E_HASH_DEFAULT;
	HMACSha256Ctx.mTagSize = CRL_SHA256_SIZE;
	HMACSha256Ctx.pmKey    = Key;
	HMACSha256Ctx.mKeySize = keyLen;

	errorStatus = HMAC_SHA256_Init(&HMACSha256Ctx);

	if (errorStatus == HASH_SUCCESS)
	{
		errorStatus = HMAC_SHA256_Append(&HMACSha256Ctx, inputMessage,
				inputMessageLength);

		if (errorStatus == HASH_SUCCESS)
		{
			errorStatus = HMAC_SHA256_Finish(&HMACSha256Ctx, messageDigest,
					messageDigestLength);

			if (errorStatus != HASH_SUCCESS) return(-1);
		}
		else return(-1);

	}
	else return(-1);

	return (1);
}

int32_t computesha256Hash(uint8_t *inputMessage, uint32_t inputMessageLength,	uint8_t *messageDigest, int32_t *messageDigestLength)
{
	SHA256ctx_stt pSHA256ctx;
	uint32_t errorStatus = HASH_SUCCESS;

	pSHA256ctx.mTagSize  = CRL_SHA256_SIZE;
	pSHA256ctx.mFlags    = E_HASH_DEFAULT;

	errorStatus = SHA256_Init(&pSHA256ctx);

	if (errorStatus == HASH_SUCCESS) {
		errorStatus = SHA256_Append(&pSHA256ctx, inputMessage,
				inputMessageLength);

		if (errorStatus == HASH_SUCCESS) {
			errorStatus = SHA256_Finish(&pSHA256ctx, messageDigest,
					messageDigestLength);
		}
	}

	return (errorStatus);
}
