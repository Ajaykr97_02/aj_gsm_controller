/*
 * hashMain.h
 *
 *  Created on: May 15, 2021
 *      Author: AJAY
 */

#ifndef CRYPTO_INC_HASHMAIN_H_
#define CRYPTO_INC_HASHMAIN_H_


#define HASH_SUCCESS             (int32_t) (0)    /*!<  hash Success */
#define HASH_ERR_BAD_OPERATION   (int32_t) (4001) /*!<  hash Invalid operation */
#define HASH_ERR_BAD_CONTEXT     (int32_t) (4002) /*!<  hash The HASH context contains some invalid or uninitialized values */
#define HASH_ERR_BAD_PARAMETER   (int32_t) (4003) /*!<  hash One of the expected function parameters is invalid */
#define HASH_ERR_INTERNAL        (int32_t) (4011) /*!<  hash Generic internal error */

#include "hmac_sha256.h"
#include "common/sha256_sha224_transform.h"
#include "sha256/sha256.h"

extern uint8_t HK[];

int32_t computeHMACsha256Hash(uint8_t *inputMessage ,uint32_t inputMessageLength ,uint8_t *messageDigest ,int32_t *messageDigestLength ,uint8_t *Key ,uint16_t keyLen);
int32_t computesha256Hash(uint8_t *inputMessage, uint32_t inputMessageLength,	uint8_t *messageDigest, int32_t *messageDigestLength);

void testSHA();
#endif /* CRYPTO_INC_HASHMAIN_H_ */
