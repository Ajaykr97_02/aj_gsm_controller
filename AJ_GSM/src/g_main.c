/*
 * g_main.c
 *
 *  Created on: Jan 7, 2021
 *      Author: AJAY
 */
#include "main.h"
#include "g_main.h"
#include "chip.h"
#include "chip_it.h"
#include "timer.h"
#include "console.h"
#include "indic.h"
#include "gsm.h"
#include "thread.h"
#include "hashMain.h"
#include "config.h"
#include "flash.h"
#include "BT.h"
#include "helper.h"
#include "network.h"


pUartHandler        U1 ,U2;
pconsoleHandler     mainConsole;

pGsmHandler         mainGsmHandle;
ackNack_hol         ackNackContainer[];
gsmResp             respHolder[];

pGsmBT              mainGsmBt;

pTcpInfohdl         mainTcpInfo;

extern consolecommands uartCliCmd[];
extern consolecommands btCliCmd[];

THREAD_FUNC(timerThreadTest)
{
	debug("TIME:%d\n" , systemTick());
	return (0);
}

void g_main(void)
{
	chipsetup();

	U1 = initUart((puartInstance)USART1_BASE , 512 , 512 ,100);
 	USART1_SetInterruptCtx(U1);
 	uartStart(U1);

	U2 = initUart((puartInstance)USART2_BASE , 512 , 512 ,100);
 	USART2_SetInterruptCtx(U2);
 	uartStart(U2);

 	mainConsole = consoleInit(U2, 512, 1000, 1 ,U1 , uartCliCmd);
 	setDebugUart(U2);

 	mainGsmHandle = gsmInit(U1 , 256 ,1024 ,U2 ,32 ,ackNackContainer ,respHolder);

 	mainGsmBt     = gsmBTInit(mainConsole ,btCliCmd);

 	addGsmBTctx(mainGsmHandle , mainGsmBt);
 	setDebugBtCtx(mainGsmHandle);

 	threadInit(64, 16, 16);

	startGpio();

	printVersion();
    setPattern(BUZZER1 , startUpPattern_A);
    setPattern(SYSTEM  , startUpPattern_A);

    debug("\nLoading configuration...\n");
    validateConfig();

    if(readPin(EC_PIN))  gsmEchoOff(mainGsmHandle);
    if(readPin(BT_PIN))  BT_PowerOn(mainGsmHandle);

    startGsmNetCheck(mainGsmHandle , 10000);

    GsmTunnel(mainGsmHandle, globalConfig.dInfo.gsmverbose);
    DEBUG_EN =  globalConfig.dInfo.verbose;

    mainTcpInfo = tcpInit(globalConfig.tInfo.serverIP, globalConfig.tInfo.port, mainGsmHandle);

	while(1)
	{
		consoleProcess(mainConsole);
		atParsetProcess(mainGsmHandle);
		indicationRoutine();
		threadRoutine();

		watchDogReset();
	}

	return;
}


/*******************************************************************************/
typedef enum ackNackList
{
	OK = 0               ,
	ACK_ERROR            ,
	SEND_OK              ,
	SEND_FAIL            ,
	SEND_SYM             ,

	PLACE_HOLDER_ACKNACK ,
	MAX_ACKNACK
}ackNack_E;


ackNack_hol ackNackContainer[MAX_ACKNACK] =
{
		ADD_ACKNACK(OK         ,TYPE_ACK),
		ADD_ACKNACK(ERROR      ,TYPE_NACK),
		ADD_ACKNACK(SEND OK    ,TYPE_ACK),
		ADD_ACKNACK(SEND FAIL  ,TYPE_NACK),
		ADD_ACKNACK(>          ,TYPE_ACK),
		{NULL}
};

/*******************************************************************************/
#if (SIM800C == 1)
void BTSPPDATA_Method(void * hdl ,uint8_t *dBuf , uint16_t len)
{
	parseSPPdata(hdl ,dBuf,len);
	return;
}

void BTCONNECTING_Method(void * hdl ,uint8_t *dBuf , uint16_t len)
{
	BT_ACPT(hdl);
	return;
}

void BTSTATUS_Method(void * hdl ,uint8_t *dBuf , uint16_t len)
{
	while(0);
	return;
}

void CPIN_Method(void * hdl ,uint8_t *dBuf , uint16_t len)
{
	parseCPIN(hdl, dBuf, len);

	return;
}

void CSQ_Method(void * hdl ,uint8_t *dBuf , uint16_t len)
{
	parseCSQ(hdl, dBuf, len);
}

void CREG_Method(void * hdl ,uint8_t *dBuf , uint16_t len)
{
	parseCREG(hdl, dBuf, len);
}

void CGATT_Method(void * hdl ,uint8_t *dBuf , uint16_t len)
{
	parseCGATT(hdl, dBuf, len);
}

void STATE_Method(void * hdl ,uint8_t *dBuf , uint16_t len)
{
	parseCIPSTATE(hdl, dBuf, len);
}

typedef enum respList
{
	BTSTATUS =0,
	BTCONNECT,
	BTSPPDATA  ,
	CPIN,
	CSQ,
	CREG,
	CGATT,
	CIPSTATE,

	PLACE_HOLDER_RESP ,
	MAX_RESP

}respList_E;

gsmResp   respHolder[MAX_RESP] =
{
		ADD_RESP(BTSTATUS),
		ADD_RESP(BTCONNECTING),
		ADD_RESP(BTSPPDATA),

		ADD_RESP(CPIN),
		ADD_RESP(CSQ),
		ADD_RESP(CREG),
		ADD_RESP(CGATT),
		ADD_RESP(STATE),
		{NULL}
};

#endif

/******************************UART CLI COMMANDS*******************************************/

CMDFUNC(ver)
{
	printVersion();
}

CMDFUNC(help)
{
	printhelpstring(mainConsole->consoleCmd);
}

CMDFUNC(reset)
{
	console("RESET INITIATED...\n");
	delayMs(10);

	systemReset();
	return;
}

CMDFUNC(verbose)
{
	char *tok ;
	uint32_t state = 0;
	tok = (char *)fetchtoken();

	pConfigHolder p = getCurrentConfig();

	if(tok && sscanf(tok , "%ld" , &state))
	{
		char *tok2;
		uint32_t state2 = 0;

		DEBUG_EN = state;
		p->dInfo.verbose = DEBUG_EN;

		tok2 = (char *)fetchtoken();

		if(tok2 && sscanf(tok2 , "%ld" , &state2))
		{
			GsmTunnel(mainGsmHandle, state2);
			p->dInfo.gsmverbose = state2;
			console("done\n");
		}
		else console("Syntax error\n");
	}

	return;
}

CMDFUNC(pattern)
{
	char *tok ;
	uint32_t gpioPin = 0;
	tok = (char *)fetchtoken();
	if(tok && sscanf(tok , "%ld" , &gpioPin))
	{
		char *tok2;
		uint32_t pattern = 0;
		tok2 = (char *)fetchtoken();

		if(tok2 && sscanf(tok2 , "%ld" , &pattern))
		{
			setPattern((IO)gpioPin ,(_PATTERN)pattern);
		}
		else console("Syntax error\n");
	}

	else console("Syntax error\n");

	return;
}

CMDFUNC(gpio)
{
	char *tok ;
	uint32_t gpioPin = 0;
	tok = (char *)fetchtoken();
	if(tok && sscanf(tok , "%ld" , &gpioPin))
	{
		char *tok2;
		uint32_t pinState = 0;
		tok2 = (char *)fetchtoken();

		if(tok2 && sscanf(tok2 , "%ld" , &pinState))
		{
			writePin((IO)gpioPin ,pinState);
		}
	}
}

CMDFUNC(gsm)
{
	uint8_t iter = 31;

	for(int i = 0 ; i < iter ; i++ )
	sendGsm(mainGsmHandle , (uint8_t *)"AT+CREG?\n" ,9 ,1000 ,3 ,CMD_NEW,NULL);

}

CMDFUNC(pin)
{
	char *tok ;
	uint32_t gpioPin = 0;
	tok = (char *)fetchtoken();

	if(tok && sscanf(tok , "%ld" , &gpioPin))
	{
		int d = getPinDir(gpioPin);
		if(d == INPUT)
			console("PIN STATE:%d",readPin(gpioPin));
	}
}

CMDFUNC(cferase)
{
	eraseConfig();
}

CMDFUNC(netstat)
{
	console("PIN %s: %d\n",printFormatSpace("PIN",12)    ,mainGsmHandle->netStat.reg.cpinStatus);
	console("SIGNALQ %s: %d\n",printFormatSpace("SIGNALQ",12),mainGsmHandle->netStat.reg.signalQuality);
	console("REG %s: %d\n",printFormatSpace("REG",12)    ,mainGsmHandle->netStat.reg.regStatus);
	console("GPRS %s: %d\n",printFormatSpace("GPRS",12)   ,mainGsmHandle->netStat.reg.gprsStatus);
	console("ipstatus %s: %d\n",printFormatSpace("ipstatus",12)  ,mainGsmHandle->netStat.reg.cipstatus);
}

CMDFUNC(config)
{
	pConfigHolder p = getCurrentConfig();

	console("DEVICE ID %s: %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n",printFormatSpace("DEVICE ID", 12), p->dInfo.devId[0], p->dInfo.devId[1], p->dInfo.devId[2],
			p->dInfo.devId[3], p->dInfo.devId[4], p->dInfo.devId[5],
			p->dInfo.devId[6], p->dInfo.devId[7]);

	console("DEBUG %s: %d,%d\n",printFormatSpace("DEBUG", 12),p->dInfo.verbose,p->dInfo.gsmverbose);
	console("APN %s: %s\n",printFormatSpace("APN", 12),p->gInfo.apn);

	console("SERVER IP %s: %d:%d:%d:%d\n",printFormatSpace("SERVER IP", 12), p->tInfo.serverIP[0],
			p->tInfo.serverIP[1], p->tInfo.serverIP[2], p->tInfo.serverIP[3]);

	console("SERVER PORT %s: %d\n",printFormatSpace("SERVER PORT", 12),p->tInfo.port);

}

CMDFUNC(setdevid)
{
	char *tok ;
	int devid[8];
	tok = (char *)fetchtoken();

	pConfigHolder p = getCurrentConfig();

	if (tok &&sscanf(tok, "%x:%x:%x:%x:%x:%x:%x:%x", &devid[0], &devid[1],
					&devid[2], &devid[3], &devid[4], &devid[5], &devid[6],
					&devid[7]))
	{
		for(int i =0 ;i<8 ;i++){
			p->dInfo.devId[i] = devid[i];
		}
		console("DEVICE ID %s: %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n",printFormatSpace("DEVICE ID", 12), p->dInfo.devId[0], p->dInfo.devId[1], p->dInfo.devId[2],
					p->dInfo.devId[3], p->dInfo.devId[4], p->dInfo.devId[5],
					p->dInfo.devId[6], p->dInfo.devId[7]);
	}
	else {
		error("INVALID FORMAT\n");
	}
}
CMDFUNC(serverip)
{
	char *tok ;
	int serverip[4];
	int port;
	tok = (char *)fetchtoken();

	pConfigHolder p = getCurrentConfig();

	if (tok && sscanf(tok, "%d.%d.%d.%d:%d", &serverip[0], &serverip[1], &serverip[2], &serverip[3],&port))
	{
		for(int i =0 ;i<4 ;i++)
		{
			p->tInfo.serverIP[i] = serverip[i];
			p->tInfo.port        = port;

		}
		console("SERVER IP %s: %d:%d:%d:%d\n",printFormatSpace("SERVER IP", 12), p->tInfo.serverIP[0],
					p->tInfo.serverIP[1], p->tInfo.serverIP[2], p->tInfo.serverIP[3]);

		console("SERVER PORT %s: %d\n",printFormatSpace("SERVER PORT", 12),p->tInfo.port);
	}
	else {
		error("INVALID FORMAT\n");
	}
}

CMDFUNC(save)
{
	pConfigHolder p = getCurrentConfig();
	updateConfig(p);
}

consolecommands uartCliCmd[] =                                                          //UART CLI commands
{
		COMMAND(ver             , This prints softwares current version),
		COMMAND(help            , Prints help string),
		COMMAND(reset           , Resets the mcu),
		COMMAND(verbose         , enable/disable interface debug),
		COMMAND(gpio            , turn on/off GPIO),
		COMMAND(pattern         , set indication pattern to gpio),
		COMMAND(gsm             , test gsm uart connection),
		COMMAND(pin             , reads pin value if input),
		COMMAND(cferase         , erases config in flash),
		COMMAND(netstat         , show gsm networks status),
		COMMAND(config          , prints configuration),
		COMMAND(setdevid        , sets device id),
		COMMAND(serverip        , sets server ip in tcp application),
		COMMAND(save            , saves changes done to configs),
		{NULL}
};

void printVersion(void)
{
	console("GSM CONTROLLER v1.0\n");
	console("%s || %s\n" , __DATE__ , __TIME__);
}
/*********************************************************************************/

BTCMDFUNC(ver)
{
	BTDEBUG("GSM CONTROLLER v1.0\r");
	BTDEBUG("%s || %s\r" , __DATE__ , __TIME__);
}

BTCMDFUNC(help)
{
	printBThelpstring(mainGsmBt->btCMD);
}

consolecommands btCliCmd[] =                                                          //BT CLI commands
{
		BTCOMMAND(ver           , This prints softwares current version),
		BTCOMMAND(help          , Prints help string),
		{NULL}
};

/***********************************************************************************/
