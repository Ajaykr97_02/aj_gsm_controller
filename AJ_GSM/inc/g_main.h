/*
 * g_main.h
 *
 *  Created on: Jan 7, 2021
 *      Author: AJAY
 */

#ifndef INC_G_MAIN_H_
#define INC_G_MAIN_H_

#include "gsm.h"
#include "uart.h"
#include "console.h"
#include "tcp.h"

#define   PIN_3  3
#define   PIN_4  4

#define   BT_PIN PIN_3     // BT on pin
#define   EC_PIN PIN_4     // echo on pin


extern pUartHandler        U1 ,U2;
extern pconsoleHandler     mainConsole;

extern pGsmHandler         mainGsmHandle;
extern pGsmBT              mainGsmBt;

extern pTcpInfohdl         mainTcpInfo;

void g_main(void);

#endif /* INC_G_MAIN_H_ */
