/*
 * config.c
 *
 *  Created on: May 22, 2021
 *      Author: AJAY
 */

#include "config.h"
#include "flash.h"
#include "hashMain.h"
#include "console.h"

configHolder defaultConfig = {{ {0} ,CONFIG_SIGNATURE , 100,0} , {defaultDevAddress ,0 ,0} , {defaultAPN} ,{defaultServerIp , 8080} };
configHolder globalConfig;

int8_t loadDefaultConfig()
{
	uint8_t digest[32];
	int32_t digestLen = 32 ;

	debug("Loading default config ..\n");

	computeHMACsha256Hash((uint8_t*)configStartAddress(defaultConfig) ,configSize ,digest ,&digestLen ,HK ,64);
	memcpy(defaultConfig.sign.hash , digest , 32);

	flashpageErase(configAddress);
	flashWrite(configAddress, (uint16_t *)&defaultConfig, sizeof(configHolder));

	memcpy(&globalConfig , &defaultConfig , sizeof(configHolder));
	return (1);
}


pConfigHolder getCurrentConfig(void)
{
	return(&globalConfig);
}

int8_t updateConfig(pConfigHolder cfg)
{
	uint8_t digest[32];
	int32_t digestLen = 32 ;

	console("updating config ..\n");

	computeHMACsha256Hash((uint8_t*)configStartAddress(*cfg) ,configSize ,digest ,&digestLen ,HK ,64);
	memcpy(cfg->sign.hash , digest , 32);

	flashpageErase(configAddress);
	flashWrite(configAddress, (uint16_t *)cfg, sizeof(configHolder));

	if(&globalConfig != cfg) memcpy(&globalConfig , cfg , sizeof(configHolder));

	console("Success\n");
	return 1;
}

int8_t validateConfig(void)
{
	configHolder cfg;

	flashRead(configAddress, (uint8_t *)&cfg, sizeof(configHolder));

	if(cfg.sign.sign == CONFIG_SIGNATURE)
	{
		uint8_t digest[32];
		int32_t digestLen = 32;

		computeHMACsha256Hash( (uint8_t *)configStartAddress(cfg), configSize, digest, &digestLen, HK, 64);

		if(memcmp(cfg.sign.hash ,digest ,digestLen) == 0)
		{
			debug("configuration load successful\n");
			memcpy(&globalConfig , &cfg , sizeof(configHolder));
		}
		else
		{
			error("Configuration signature verification failed\n");
			loadDefaultConfig();
			return (-1);
		}
	}
	else
	{
		error("Configuration not found in flash\n");
		loadDefaultConfig();
		return (-1);
	}
	return (1);
}

void eraseConfig(void)
{
	flashpageErase(configAddress);
}
