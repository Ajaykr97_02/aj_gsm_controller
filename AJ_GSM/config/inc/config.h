/*
 * config.h
 *
 *  Created on: May 22, 2021
 *      Author: AJAY
 */

#ifndef CONFIG_INC_CONFIG_H_
#define CONFIG_INC_CONFIG_H_

#include "stdint.h"

#define CONFIG_SIGNATURE        0x7f7f7f7f
#define configAddress           0x8000400
#define defaultDevAddress       {0xff ,0xff ,0xff ,0xff ,0xff ,0xff ,0xff ,0xff}
#define defaultAPN              "airtelgprs.com"

#define defaultServerIp         {0xFF , 0xFF , 0xFF , 0xFF}

#define configStartAddress(CTX) ((uint32_t)(&CTX)+sizeof(configSign))
#define configSize              (sizeof(configHolder)-sizeof(configSign))

typedef struct configSignature_st
{
	uint8_t  hash[32];
	uint32_t sign;
	uint16_t version;
	uint16_t configLen;
}configSign;

typedef struct deviceInformation_st
{
	uint8_t  devId[8];
	uint16_t verbose;
	uint16_t gsmverbose;
}devInfo;

typedef struct gsmInformation_st
{
	uint8_t apn[32];

}gsmInfo;

typedef struct tcpInformation_st
{
	uint8_t   serverIP[4];
	uint16_t  port;
}tcpInfo , *pTcpInfo;

typedef struct configHolder_st
{
	configSign  sign;
	devInfo     dInfo;
	gsmInfo     gInfo;
	tcpInfo     tInfo;

}configHolder ,*pConfigHolder;

extern configHolder globalConfig;

pConfigHolder getCurrentConfig(void);
int8_t validateConfig(void);
int8_t updateConfig(pConfigHolder cfg);

void eraseConfig(void);

#endif /* CONFIG_INC_CONFIG_H_ */
