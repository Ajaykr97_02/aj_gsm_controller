/*
 * indication.c
 *
 *  Created on: Jan 13, 2021
 *      Author: AJAY
 */
#include "indic.h"
#include "console.h"

indic_Inf indInf[IO_MAX] = {0};

int32_t patternHolder[PATTERN_MAX][32] =
{
		{1000 ,100 ,50 ,200 ,100 ,300 ,200 ,100                                                   ,INDIC_TIMER , 1 } ,
		{100 ,100                                                                                 ,INDIC_COUNT , 3 },
		{100 ,100 , 200 ,200 ,300 ,300                                                            ,INDIC_COUNT , 3 },
		{50 ,50                                                                                   ,INDIC_COUNT , 20},
		{50 ,50 ,50 ,50 ,50 ,50 ,50 ,50 ,50 ,50 ,100 ,100 ,100 ,100 ,100 ,100 ,100 ,100 ,100 ,100 ,INDIC_COUNT , 20},
};

void setPattern(IO pin , _PATTERN pattern)
{
	if(pattern >= PATTERN_MAX)     {debug("Unknown pattern:%d\n",pattern); return;}
	if(getPinDir(pin) != OUTPUT)   {debug("Invalid pin direction\n");      return;}

	indInf[pin].pattern        = patternHolder[pattern];
	indInf[pin].lastTime       = systemTick();
	indInf[pin].startTime      = systemTick();
	indInf[pin].active         = _SET;
	indInf[pin].Idx            = _RESET;
	indInf[pin].overFlowCount  = _RESET;

	setDefaultState(pin);
	return;
}

void indicationRoutine(void)
{
	for(int i = 0 ;i < IO_MAX ; i++)
	{
		if(!indInf[i].active) continue;

		if((systemTick() - indInf[i].lastTime) >= indInf[i].pattern[indInf[i].Idx])
		{
			togglePin(i);
			indInf[i].Idx++;
			indInf[i].lastTime = systemTick();

			if(indInf[i].pattern[indInf[i].Idx] == INDIC_REPEAT) { indInf[i].Idx = 0;  indInf[i].overFlowCount++;}

			else if(indInf[i].pattern[indInf[i].Idx] == INDIC_TIMER)
			{
				indInf[i].overFlowCount++;
				if((systemTick() - indInf[i].startTime) >= indInf[i].pattern[indInf[i].Idx+1]) {indInf[i].active = _RESET; continue;}
				else  { indInf[i].Idx = 0;}
			}

			else if(indInf[i].pattern[indInf[i].Idx] == INDIC_COUNT)
			{
				indInf[i].overFlowCount++;
				if(indInf[i].overFlowCount >= indInf[i].pattern[indInf[i].Idx+1]) {indInf[i].active = _RESET; continue;}
				else  {indInf[i].Idx = 0;  }
			}
		}
	}
	return;
}
