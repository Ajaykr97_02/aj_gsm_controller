/*
 * list_l.c
 *
 *  Created on: Dec 13, 2019
 *      Author: AJAY
 */

#include <stdint.h>
#include "list_l.h"
#include <stdlib.h>
#include "chip_it.h"
#include "string.h"

void listadd_deprecated(llistprocto_t **head, pllistprotoc add) {

	pllistprotoc header = *head;

	if (!header) {
		add->next = NULL;
	} else {
		add->next = header;
	}

	*head = add;

}

void listadd(llistprocto_t **head, pllistprotoc add) {

	pllistprotoc header = *head;

	if (!header) {
		add->next = NULL;
		*head = add;
	} else {
		pllistprotoc p = listtail(head);
		p->next = add;
	}

}

pllistprotoc listtail(pllistprotoc *listmem) {

	pllistprotoc iter = *listmem;
	if (!*listmem)
		return 0;

	for (iter = *listmem; iter->next != NULL; iter = iter->next);

	return iter;
}

int allocatememory(void **alloc, uint32_t size) {
	*alloc = malloc(size);
	if (!*alloc) {
		return -1;
	}

	memset(*alloc, 0, size);
	return 0;
}

