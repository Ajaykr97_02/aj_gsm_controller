/*
 * helper.c
 *
 *  Created on: Dec 21, 2019
 *      Author: AJAY
 */
#include "helper.h"
#include "uart.h"
#include "bt.h"

void setpramtoken(uint8_t *strng , int len , char delim)
{
	tokenstring  = strng;
	tokenstrlen = len;
	delimiter = delim;
}

uint8_t* fetchtoken()
{
	uint8_t *p = tokenstring;
	uint8_t *rtoken;
	if(*p == '\0')
	{
		return NULL;
	}
	while(*p == ' ')
	{
		p++;
	}

	if(*p == '\n' || *p == '\r') return NULL;
	rtoken = p;
	for(int i = 0 ; i< tokenstrlen ; i++)
	{
		if(*p == ' ')
		{
			*p = '\0';
			tokenstring = p+1;
			return (rtoken);
		}
		else if( *p == '\n' || *p == '\r')
		{
			*p = '\0';
			tokenstring = p;
			return (rtoken);
		}
		else{
			p++;
		}
	}
	return NULL;
}

#define MAX_SPACES 32
char spaceBuf[MAX_SPACES] = {0};

char *printFormatSpace(char *str , uint8_t spaces)
{
	if(spaces > MAX_SPACES) spaces = MAX_SPACES;

	memset((void *)spaceBuf ,0 ,32);
	memset((void *)spaceBuf ,0x20 ,(spaces - strlen(str)));

	return (spaceBuf);
}

#ifdef  _PRINTF_CUSTOM

char printBuf[128] = {0};

pUartHandler DEBUGUART;
pGsmHandler  DEBUGGSM;

void setDebugUart(pUartHandler u)
{
	DEBUGUART = u;
}

void PRINTF(char *fmt, ...)
{
	if(!DEBUGUART) return;
    va_list args;

    va_start(args, fmt);

    vsprintf((char *)printBuf, fmt, args);
    va_end(args);

    putStreamSerial(DEBUGUART ,(uint8_t *)printBuf, strlen(printBuf));
}

void setDebugBtCtx(pGsmHandler u)
{
	DEBUGGSM = u;
}

void BTPRINTF(char *fmt, ...)
{
	if(!DEBUGGSM) return;
    va_list args;

    va_start(args, fmt);

    vsprintf((char *)printBuf, fmt, args);
    va_end(args);

    sendSPPData(DEBUGGSM, (uint8_t *)printBuf, strlen(printBuf));

}
#endif
