/*
 * healper.h
 *
 *  Created on: Dec 21, 2019
 *      Author: AJAY
 */
/***************************************************************************/
#ifndef HELPER_H_
#define HELPER_H_

#include "stdint.h"
#include "stddef.h"
#include "string.h"
#include "stdio.h"
#include "stdarg.h"
#include "uart.h"


#define _PRINTF_CUSTOM
/***************************************************************************/
#define __DEBUG(fmt, ...)                   printf(fmt, ##__VA_ARGS__)

#define   mem32(x)                       ((volatile uint32_t *)(x))
#define   mem16(x)                       ((volatile uint16_t *)(x))

#define   CAT2(x, y, z)                   x##y##z

#define   STR(x)                          #x
#define   STR2(x)                         STR(x)

#define   bit(bit) (1 << bit)
#define   bit_is_set(val, bit)            (((val & (1 << bit)) >> bit) == 1)
#define   clear_bit(val, bit)             (val = (val & ~(1 << bit)))
#define   set_bit(val, bit)               (val = (val | (1 << bit)))

#define   ones(num)                       ( (1ULL << num) - 1 )
#define   bit_mask(length, shift)         (ones(length) << shift)
#define   get_field(val, field)           ((val & field##_MASK) >> field)
#define   bitfield(name, length, shift)   ( define #name length )

#define   CHARISNUM(x)                    ((x) >= '0' && (x) <= '9')
#define   CHARISHEXNUM(x)                 (((x) >= '0' && (x) <= '9') || ((x) >= 'a' && (x) <= 'f') || ((x) >= 'A' && (x) <= 'F'))
#define   CHARTONUM(x)                    ((x) - '0')
#define   CHARHEXTONUM(x)                 (((x) >= '0' && (x) <= '9') ? ((x) - '0') : (((x) >= 'a' && (x) <= 'z') ? ((x) - 'a' + 10) : (((x) >= 'A' && (x) <= 'Z') ? ((x) - 'A' + 10) : 0)))
#define   FROMMEM(x)                      ((const char *)(x))

#define   RB_FREESPACE(start , end , bufferSize)  ((end>start)?(end-start):(bufferSize -(start -end)))
/***************************************************************************/

uint8_t     *tokenstring;
int         tokenstrlen;
char        delimiter;
uint8_t     *tokens;
char        tokencount;

void        setpramtoken(uint8_t *strng , int len , char delim);
uint8_t*    fetchtoken();
char        *printFormatSpace(char *cmd , uint8_t spaces);

void        setDebugUart(pUartHandler u);
void        PRINTF(char *fmt, ...);

/***************************************************************************/
#endif /* HELPER_H_ */
