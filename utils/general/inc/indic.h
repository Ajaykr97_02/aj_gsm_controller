/*
 * indic.h
 *
 *  Created on: Jan 13, 2021
 *      Author: AJAY
 */

#ifndef GENERAL_INC_INDIC_H_
#define GENERAL_INC_INDIC_H_

#include "stdint.h"
#include "gpio.h"
#include "timer.h"

typedef int32_t* PATTERN;

#define INDIC_REPEAT -1
#define INDIC_TIMER  -2
#define INDIC_COUNT  -3

typedef enum
{
	startUpPattern_A =0,
	ERRORPATTERN_A,
	startUpPattern_C,
	startUpPattern_D,
	startUpPattern_E,
	PATTERN_MAX
}_PATTERN;

typedef struct indicationInfo_st
{
	uint8_t    active;
	PATTERN    pattern;
	clockTime  lastTime;
	clockTime  startTime;
	uint16_t   overFlowCount;
	uint16_t   Idx;

}indic_Inf, *pIndic_Inf;

/*(OFF duration ,ON duration ..,INDIC_TYPE ,OPTIONAL INDICATION WORD)*/
extern int32_t patternHolder[][32];

void setPattern(IO pin , _PATTERN pattern);
void indicationRoutine(void);


#endif /* GENERAL_INC_INDIC_H_ */
