/*
 * list_l.h
 *
 *  Created on: Dec 13, 2019
 *      Author: AJAY
 */

#ifndef LIST_L_H_
#define LIST_L_H_


typedef struct llistprotoc_
{
	struct llistprotoc_ *next;

}llistprocto_t , *pllistprotoc;


void listadd(llistprocto_t **head  , pllistprotoc add);
int allocatememory(void **alloc , uint32_t size );

pllistprotoc listtail(pllistprotoc *listmem);

#endif /* LIST_L_H_ */
