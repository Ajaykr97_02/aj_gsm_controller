/*
 * thread.c
 *
 *  Created on: May 5, 2021
 *      Author: AJAY
 */

#include "thread.h"
#include "console.h"
#include "stdlib.h"


void executeThread(thread *T);


pThread orderedThread  = {0};
pThread freeflowThread = {0};
pThread timerThread    = {0};

int threadInit(uint16_t MAX_OrderedThread , uint16_t MAX_FreeFlowThread , uint16_t MAX_TimerThread)
{
	pThread OT = calloc(1  , sizeof(thread));
	if(!OT) return(-1);

	OT->maxThread   = MAX_OrderedThread;
	OT->threadList  = calloc(MAX_OrderedThread , sizeof(thread_h));
	OT->threadType  = ORDERED_THREAD;

	orderedThread   = OT;

	pThread FT = calloc(1  , sizeof(thread));
	if(!FT) return(-1);

	FT->maxThread   = MAX_FreeFlowThread;
	FT->threadList  = calloc(MAX_FreeFlowThread, sizeof(thread_h));
	FT->threadType  = FREEFLOW_THREAD;

	freeflowThread  = FT;

	pThread TT = calloc(1  , sizeof(thread));
	if(!TT) return(-1);

	TT->maxThread        = MAX_TimerThread;
	TT->timerThreadList  = calloc(MAX_TimerThread, sizeof(timerThread_h));
	TT->threadType       = TIMER_THREAD;

	timerThread     = TT;

	return (1);
}

pThread_h getFreeThread(pThread T)
{
	if(!T)              return(0);
	if(!T->threadList)  return(0);

	for(int i = 0; i < T->maxThread ; i++){

		if(!T->threadList[i].active) return (&T->threadList[i]);
	}
	return (0);
}

pTimerThread_h getFreeTimerThread(pThread T)
{
	if(!T)                   return(0);
	if(!T->timerThreadList)  return(0);

	for(int i = 0; i < T->maxThread ; i++)
	{
		if(!T->timerThreadList[i].active) return (&T->timerThreadList[i]);
	}
	return (0);
}


int loadThread(threadFunc func , void *args , uint8_t threadType , uint32_t timeout)
{
	if(threadType == ORDERED_THREAD){

		if(orderedThread->threadList[orderedThread->currentThreadLoad].active)  return (-1);

		orderedThread->threadList[orderedThread->currentThreadLoad].thread = func;
		orderedThread->threadList[orderedThread->currentThreadLoad].args   = args;

		orderedThread->threadList[orderedThread->currentThreadLoad].active = 1;
		orderedThread->currentThreadLoad++;

		if(orderedThread->currentThreadLoad >= orderedThread->maxThread)
		{
              orderedThread->currentThreadLoad=0;
		}
	}

	else if(threadType == FREEFLOW_THREAD)
	{
		pThread_h p = getFreeThread(freeflowThread);

		if(!p) return (-1);

		p->thread = func;
		p->args   = args;
		p->active = 1;

	}
	else if(threadType == TIMER_THREAD)
	{
		pTimerThread_h p = getFreeTimerThread(timerThread);
		if(!p) return (-1);

		p->thread   = func;
		p->args     = args;
		p->lastTime = systemTick();
		p->timeout  = timeout;
		p->active   = 1;
	}

	return (1);
}

void executeThread(thread *T)
{
	int a = 0;
	if(T->threadType == ORDERED_THREAD)
	{
		if(!T->threadList) return;

		while(T->currentThreadExecute != T->currentThreadLoad)
		{
			if(!T->threadList[T->currentThreadExecute].active) return;

			if((a = T->threadList[T->currentThreadExecute].thread(T->threadList[T->currentThreadExecute].args)) == 1)
			{
				T->threadList[T->currentThreadExecute].active = 0;
				T->currentThreadExecute++;

				if(T->currentThreadExecute >= T->maxThread)
				{
					T->currentThreadExecute = 0;
				}
			}
			else return;
		}
	}

	else if(T->threadType == FREEFLOW_THREAD)
	{
		if(!T->threadList) return;

		for(int i = 0 ; i < T->maxThread ; i++)
		{
			if(!T->threadList[i].active) continue;

			if((T->threadList[i].thread(T->threadList[i].args)) == 1)
			{
				T->threadList[i].active = 0;
			}
		}
	}

	else if(T->threadType == TIMER_THREAD)
	{
		if(!T->timerThreadList) return;

		for(int i = 0 ; i < T->maxThread ; i++)
		{
			if(!T->timerThreadList[i].active) continue;

			uint32_t t = systemTick();

			if((t - T->timerThreadList[i].lastTime) >= T->timerThreadList[i].timeout)
			{
				T->timerThreadList[i].lastTime  = t;

				if((T->timerThreadList[i].thread(T->timerThreadList[i].args)) == 1)
				{
					T->timerThreadList[i].active = 0;
				}
			}
		}
	}
	return;
}

void threadRoutine(void)
{
	if(orderedThread)	executeThread(orderedThread);
	if(freeflowThread)  executeThread(freeflowThread);
	if(timerThread)	    executeThread(timerThread);
}
