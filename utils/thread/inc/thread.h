/*
 * thread.h
 *
 *  Created on: May 5, 2021
 *      Author: AJAY
 */

#ifndef THREAD_INC_THREAD_H_
#define THREAD_INC_THREAD_H_

#include "stdint.h"

#define NA               0

#define ORDERED_THREAD   1
#define FREEFLOW_THREAD  2
#define TIMER_THREAD     3

#define THREAD_FUNC(NAME) int NAME(void *args)

typedef int (*threadFunc)(void *args);


typedef struct threadHandler_ {
	threadFunc thread;
	void       *args;
	uint8_t    active;

} thread_h, *pThread_h;

typedef struct timerTthreadHandler_
{
	threadFunc thread;
	void       *args;
	uint8_t    active;
	uint32_t   timeout;
	uint32_t   lastTime;

} timerThread_h, *pTimerThread_h;

typedef struct thread_st
{
	pThread_h       threadList;
	pTimerThread_h  timerThreadList;

	uint8_t         currentThreadLoad;
	uint8_t         currentThreadExecute;
	uint16_t        maxThread;
	uint8_t         threadType;

} thread, *pThread;


int threadInit(uint16_t MAX_OrderedThread , uint16_t MAX_FreeFlowThread , uint16_t MAX_TimerThread);

int loadThread(threadFunc func , void *args , uint8_t threadType , uint32_t timeout);
void threadRoutine(void);

#endif /* THREAD_INC_THREAD_H_ */
