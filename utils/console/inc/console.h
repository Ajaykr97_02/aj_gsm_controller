/*
 * console.h
 *
 *  Created on: Jan 7, 2021
 *      Author: AJAY
 */

#ifndef CONSOLE_INC_CONSOLE_H_
#define CONSOLE_INC_CONSOLE_H_

#include "stdint.h"
#include "uart.h"
#include "helper.h"
#include "indic.h"

#define CMDFUNC(NAME) void NAME ## _method(void *hdl)
#define COMMAND(NAME , HELPSTRING)  { #NAME, #HELPSTRING, NAME ## _method }

#define BTCMDFUNC(NAME) void NAME ## _BTmethod(void *hdl)
#define BTCOMMAND(NAME , HELPSTRING)  { #NAME, #HELPSTRING, NAME ## _BTmethod }

#define console PRINTF

#define debug   if(DEBUG_EN) PRINTF
#define error   setPattern(SYSTEM , ERRORPATTERN_A); \
PRINTF

typedef void (*interpretorFuncPtr)(void * ,uint8_t* , uint16_t);
typedef void (*msgfuncptr)(void *hdl);

typedef struct consolecommands_
{
	char *commandstr;
	char *helpstr;
	msgfuncptr func;

}consolecommands , *pconsolecommand;

typedef struct consoleHandler_st
{
	pUartHandler         uctx;
	uint8_t              *buf;
	uint16_t             index;
	uint16_t             maxBufLen;
	uint16_t             timeout;
	uint8_t              verboseMode;
	interpretorFuncPtr   asciiInterpretor;
	interpretorFuncPtr   binaryInterpretor;
	pUartHandler         tunnelCtx;
	pconsolecommand      consoleCmd;
}consoleHandler , *pconsoleHandler;

extern uint8_t DEBUG_EN;

pconsoleHandler consoleInit(pUartHandler pUCtx , uint16_t bufLen , uint16_t timeout , uint16_t verbose , pUartHandler capTunnelCtx , pconsolecommand cmd);
void consoleProcess(pconsoleHandler conH);

void printhelpstring(pconsolecommand cmd);
void printVersion(void);

#endif /* CONSOLE_INC_CONSOLE_H_ */
