/*
 * console.c
 *
 *  Created on: Jan 7, 2021
 *      Author: AJAY
 */

#include "console.h"
#include "g_main.h"
#include "helper.h"
#include "chip.h"
#include "timer.h"
#include "gpio.h"
#include "indic.h"
#include "thread.h"
#include "gsm.h"
#include "string.h"
#include "config.h"
#include "stdlib.h"


void consoleAsciiInterpretor(void *hdl  ,uint8_t* dBuf , uint16_t dLen);
void consoleBinaryInterpretor(void *hdl ,uint8_t* dBuf , uint16_t dLen);

uint8_t DEBUG_EN = 1;

pconsoleHandler consoleInit(pUartHandler pUCtx , uint16_t bufLen , uint16_t timeout , uint16_t verbose , pUartHandler capTunnelCtx , pconsolecommand cmd)
{

	pconsoleHandler c = calloc(1 , sizeof(consoleHandler));

	if(!c)              return (0);

	c->uctx           = pUCtx;
	c->buf            = calloc(1 , bufLen);
	if(!c->buf)       { free(c);  return (0);}

	c->maxBufLen      = bufLen;
	c->verboseMode    = verbose;
	c->timeout        = timeout;
	c->tunnelCtx      = capTunnelCtx;
	c->consoleCmd     = cmd;

	c->asciiInterpretor  = consoleAsciiInterpretor;
	c->binaryInterpretor = consoleBinaryInterpretor;

	return (c);
}

void consoleProcess(pconsoleHandler conH)
{
	int ch = getCharSerial(conH->uctx);

	if(ch == -1){ return; }

	conH->buf[conH->index] = ch;
	conH->index = (conH->index +1) & (conH->maxBufLen - 1);

	if(ch == '\n' || ch == '\r')
	{
		conH->asciiInterpretor(conH ,conH->buf , conH->index);
		conH->index = 0;
	}
	else if(ch == 8 || ch == 127)
	{
		conH->index = conH->index - 2;
		putStreamSerial(conH->uctx, (uint8_t*)"\b \b",3);
	}
	else
	{
		if(!conH->verboseMode) return;
		putCharSerial(conH->uctx, ch);
	}
	return;
}


void consoleAsciiInterpretor(void *hdl ,uint8_t* dBuf , uint16_t dLen)
{
	pconsoleHandler con = (pconsoleHandler)hdl;
	putCharSerial((pUartHandler)con->uctx, '\n');

	if(dBuf[0] == '^')
	{
		if(!con->tunnelCtx) {debug("Cap tunnel disabled\n"); return;}

		putStreamSerial(con->tunnelCtx, &dBuf[1], (dLen-1));
		return;
	}
	setpramtoken(dBuf, dLen, ' ');

	while(1)
	{
		tokens = fetchtoken();

		if(tokens == NULL) break;
		tokencount++;
		for(pconsolecommand iter = con->consoleCmd ;  iter->commandstr ; iter++)
		{
			if(strcmp(iter->commandstr , (char*)tokens) == 0)
			{
				iter->func(hdl);
				return;
			}
		}

		putCharSerial((pUartHandler)con->uctx, '\n');
		error("SYNTAX ERROR\n");
		break;
	}
}

void consoleBinaryInterpretor(void *hdl ,uint8_t* dBuf , uint16_t dLen)
{

}

void printhelpstring(pconsolecommand cmd)
{
	for(pconsolecommand cmditer = &cmd[0]; cmditer->commandstr;cmditer++)
	{
		console("%s %s:%s\n" , cmditer->commandstr,printFormatSpace(cmditer->commandstr,12),cmditer->helpstr);
	}

	debug("\n");
}

/*********************************************************************************/

