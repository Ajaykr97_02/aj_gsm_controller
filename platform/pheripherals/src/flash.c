/*
 * flash.c
 *
 *  Created on: May 21, 2021
 *      Author: AJAY
 */

#include "flash.h"
#include "stm32f103xb.h"
#include "stm32f1xx.h"
#include "string.h"

int flashpageErase(uint32_t PageAddress)
{
	flashUnlock();

	SET_BIT(FLASH->CR, FLASH_CR_PER);
	WRITE_REG(FLASH->AR, PageAddress);
	SET_BIT(FLASH->CR, FLASH_CR_STRT);

	while(FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

	CLEAR_BIT(FLASH->CR, FLASH_CR_PER);

	flashLock();
	return (1);
}

int8_t flashWrite(uint32_t address , uint16_t *data , uint32_t length)
{

	flashUnlock();

	uint16_t l = (length/2) + (length%2?1:0);
	uint32_t n;
	uint8_t *ptr = (uint8_t*) address;
	uint16_t *d = data;

	for(n=0;n<l;n++)
	{
		FLASH->CR |= FLASH_TYPEPROGRAM_HALFWORD;
		FLASH->CR |= FLASH_CR_PG;

		*(volatile uint16_t*)ptr= *d++;
		  ptr += 2;

		  while(FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);
	}

	FLASH->CR &= (~FLASH_CR_PG);

	flashLock();

	return (1);
}

int8_t flashRead( uint32_t address , uint8_t *dest , uint16_t length)
{
	uint8_t *s = (uint8_t *) address;
	memcpy(dest , s , length);

	return (1);
}

int flashUnlock()
{
	int status = 0;

	if(READ_BIT(FLASH->CR, FLASH_CR_LOCK) != RESET)
	{
		WRITE_REG(FLASH->KEYR, FLASH_KEY1);
		WRITE_REG(FLASH->KEYR, FLASH_KEY2);

		if(READ_BIT(FLASH->CR, FLASH_CR_LOCK) != RESET)
		{
			status = 1;
		}
	}

	return (status);
}

int flashLock()
{
	  SET_BIT(FLASH->CR, FLASH_CR_LOCK);
	  return (0);
}
