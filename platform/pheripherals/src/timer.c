/*
 * timer.c
 *
 *  Created on: Jan 8, 2021
 *      Author: AJAY
 */

#include "chip_it.h"
#include "chip.h"
#include "timer.h"

clockTime systemTick(void)
{
	return getSysTick();
}

clockTime systemTime(void)
{
	return(systemTick()/1000);
}

void delayMs(clockTime milliSec)
{
	uint32_t p = systemTick();

	while((systemTick() - p) < milliSec){  WDRESET; }

}



