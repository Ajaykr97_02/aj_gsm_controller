/*
 * uart.c
 *
 *  Created on: Jan 5, 2021
 *      Author: AJAY
 */

#include "uart.h"
#include "stm32f103xb.h"
#include "stm32f1xx.h"
#include "stdlib.h"
#include "helper.h"


pUartHandler initUart(puartInstance uartInstance , uint16_t rxBufLen , uint16_t txBufLen , uint16_t timeout)
{
	pUartHandler u = calloc(1, sizeof(uartHandler));

	if(!u)           return(0);
	u->Instance    = uartInstance;

	u->rxBuf       = calloc(1 ,rxBufLen);
	if(!u->rxBuf)  { free(u); return(0);}
	u->maxRxBufLen = rxBufLen;

	u->txBuf       = calloc(1, txBufLen);
	if(!u->txBuf)  { free(u); return(0);}
	u->maxTxBufLen = txBufLen;

	u->timeout     = timeout;
	u->trigger     = 1;

	return (u);
}

void uartStart(pUartHandler u_h)
{
	__HAL_UART_ENABLE_IT(u_h, UART_IT_PE);
	__HAL_UART_ENABLE_IT(u_h, UART_IT_ERR);
	__HAL_UART_ENABLE_IT(u_h, UART_IT_RXNE);
}

int getCharSerial(pUartHandler uh)
{
	if(uh->rx_g == uh->rx_p) return (-1);

	int ch =  uh->rxBuf[uh->rx_g];
	uh->rx_g = (uh->rx_g +1);

	if(uh->rx_g == uh->maxRxBufLen)  uh->rx_g = 0;

	return (ch);
}

int putCharSerial(pUartHandler uh , uint8_t ch)
{
	while(RB_FREESPACE(uh->tx_p, uh->tx_g , uh->maxTxBufLen) < 5)
	{
		((IWDG_TypeDef *)IWDG)->KR = 0x0000AAAA;
	}

	uh->txBuf[uh->tx_p] = ch;
	uh->tx_p = (uh->tx_p + 1);

	if(uh->tx_p == uh->maxTxBufLen) uh->tx_p  = 0;

	if(uh->trigger)
	{
		uh->trigger = 0;
		__HAL_UART_ENABLE_IT(uh, UART_IT_TXE);
	}
	return (1);
}

int putStreamSerial(pUartHandler uh , uint8_t *dBuf , uint16_t dLen )
{
	for(int i = 0 ; i < dLen ; i++)	putCharSerial(uh, dBuf[i]);

	return (1);
}
