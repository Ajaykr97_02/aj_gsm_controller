/*
 * gpio.c
 *
 *  Created on: Jan 5, 2021
 *      Author: AJAY
 */
#include "gpio.h"
#include "console.h"
#include "stm32f1xx.h"
#include "helper.h"

gpioInfo gInfo[IO_MAX+1] =
{
		ADDGPIO(GPIOB_BASE ,GPIO_PIN_3 , OUTPUT ,0),
		ADDGPIO(GPIOB_BASE ,GPIO_PIN_4 , OUTPUT ,0),
		ADDGPIO(GPIOB_BASE ,GPIO_PIN_1 , OUTPUT ,0),
		ADDGPIO(GPIOA_BASE ,GPIO_PIN_7 , INPUT  ,0),
		ADDGPIO(GPIOB_BASE ,GPIO_PIN_0 , INPUT  ,0),

		{NULL}
};


void startGpio(void)
{
	for(pgpioInfo gInf = &gInfo[0]; gInf->Instance ; gInf++)
	{
		if(gInf->pinDir == OUTPUT)
		{
			if(gInf->defaultState) PINSET(gInf);
			else                   PINRESET(gInf);
		}
	}
}

void togglePin(IO pin)
{
	PINTOGGLE(&gInfo[pin]);
}

void writePin(IO pin , uint8_t state)
{
	if(pin >= IO_MAX || gInfo[pin].pinDir != OUTPUT) {debug("UNKNOWN PIN:%d\n",pin); return;}

	if(state)	PINSET(&gInfo[pin]);
	else        PINRESET(&gInfo[pin]);
}

int8_t readPin(IO pin)
{
	if(pin >= IO_MAX || gInfo[pin].pinDir != INPUT) {debug("UNKNOWN PIN:%d\n",pin); return -1;}
	return (PINGET(&gInfo[pin]));

}

uint8_t getPinDir(IO pin)       { return(gInfo[pin].pinDir); }
void setDefaultState(IO pin)    { writePin(pin , gInfo[pin].defaultState); }
uint8_t getDefaultState(IO pin) { return (gInfo[pin].defaultState);}
