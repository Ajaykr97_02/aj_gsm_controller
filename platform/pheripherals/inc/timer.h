/*
 * timer.h
 *
 *  Created on: Jan 8, 2021
 *      Author: AJAY
 */

#ifndef PHERIPHERALS_INC_TIMER_H_
#define PHERIPHERALS_INC_TIMER_H_

#include "stdint.h"

typedef uint32_t clockTime;

clockTime systemTick(void);
clockTime systemTime(void);
void delayMs(clockTime milliSec);

#endif /* PHERIPHERALS_INC_TIMER_H_ */
