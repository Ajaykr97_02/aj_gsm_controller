/*
 * uart.h
 *
 *  Created on: Jan 5, 2021
 *      Author: AJAY
 */
#ifndef PHERIPHERALS_INC_UART_H_
#define PHERIPHERALS_INC_UART_H_

#include "stdint.h"


typedef struct consoleInstance_st
{

  volatile uint32_t SR;
  volatile uint32_t DR;
  volatile uint32_t BRR;
  volatile uint32_t CR1;
  volatile uint32_t CR2;
  volatile uint32_t CR3;
  volatile uint32_t GTPR;

} uartInstance , *puartInstance ;

typedef struct uartHandler_ST
{
	puartInstance Instance;

	uint32_t     lastChar;
	uint8_t      *rxBuf;
	uint8_t      *txBuf;
	uint16_t     rx_p;
	uint16_t     rx_g;
	uint16_t     tx_p;
	uint16_t     tx_g;
	uint16_t     trigger;
	uint16_t     maxRxBufLen;
	uint16_t     maxTxBufLen;
	uint16_t     timeout;

}uartHandler , *pUartHandler;

pUartHandler initUart(puartInstance uartInstance , uint16_t rxBufLen , uint16_t txBufLen , uint16_t timeout);
void uartStart(pUartHandler u_h);

int getCharSerial(pUartHandler Uhandler);
int putCharSerial(pUartHandler uh , uint8_t ch);
int putStreamSerial(pUartHandler uh , uint8_t *dBuf , uint16_t dLen );

#endif

