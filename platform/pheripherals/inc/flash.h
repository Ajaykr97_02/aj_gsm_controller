/*
 * flash.h
 *
 *  Created on: May 21, 2021
 *      Author: AJAY
 */

#ifndef PHERIPHERALS_INC_FLASH_H_
#define PHERIPHERALS_INC_FLASH_H_

#include "stdint.h"

#define FLASH_GET_FLAG(__FLAG__)  (((__FLAG__) == FLASH_FLAG_OPTVERR) ? \
                                            (FLASH->OBR & FLASH_OBR_OPTERR) : \
                                            (FLASH->SR & (__FLAG__)))
void testFlash();

int flashpageErase(uint32_t PageAddress);
int8_t flashWrite(uint32_t address , uint16_t *data , uint32_t length);
int8_t flashRead( uint32_t address , uint8_t *dest , uint16_t length);

int flashUnlock();
int flashLock();

#endif /* PHERIPHERALS_INC_FLASH_H_ */
