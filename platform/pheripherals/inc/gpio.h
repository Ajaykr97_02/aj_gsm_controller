/*
 * gpio.h
 *
 *  Created on: Jan 5, 2021
 *      Author: AJAY
 */
#ifndef PHERIPHERALS_INC_GPIO_H_
#define PHERIPHERALS_INC_GPIO_H_

#include "stdint.h"

#define INPUT  1
#define OUTPUT 2
#define EXTINT 3

#define ADDGPIO(INSTANCE ,PIN , DIR ,DEFAULTSTATE)   { (pgpioInstance)INSTANCE ,PIN ,DIR ,DEFAULTSTATE}

typedef enum IO
{
	SYSTEM = 0,
	RELAY1,
	BUZZER1,
	MODESEL0,
	MODESEL1,
	IO_MAX
}IO;

enum
{
  _RESET = 0,
  _SET = !_RESET
};


#define PINGET(GPIOPIN)     (((pgpioInfo)GPIOPIN)->Instance->IDR) &  (((pgpioInfo)GPIOPIN)->pinNumber)?1:0

#define PINSET(GPIOPIN)     ((pgpioInfo)GPIOPIN)->Instance->BSRR = ((pgpioInfo)GPIOPIN)->pinNumber
#define PINRESET(GPIOPIN)   ((pgpioInfo)GPIOPIN)->Instance->BSRR = ((pgpioInfo)GPIOPIN)->pinNumber << 16u
#define PINTOGGLE(GPIOPIN)  ((pgpioInfo)GPIOPIN)->Instance->BSRR = ((((pgpioInfo)GPIOPIN)->Instance->ODR & ((pgpioInfo)GPIOPIN)->pinNumber) << 16u) | (~(((pgpioInfo)GPIOPIN)->Instance->ODR) & ((pgpioInfo)GPIOPIN)->pinNumber)

typedef struct
{
  volatile uint32_t CRL;
  volatile uint32_t CRH;
  volatile uint32_t IDR;
  volatile uint32_t ODR;
  volatile uint32_t BSRR;
  volatile uint32_t BRR;
  volatile uint32_t LCKR;
} gpioInstance , *pgpioInstance;

typedef struct gpioInfo_st
{
	gpioInstance *Instance;
	uint8_t      pinNumber;
	uint8_t      pinDir;
	uint8_t      defaultState;
}gpioInfo , *pgpioInfo;

void    startGpio(void);
void    writePin(IO pin , uint8_t state);
int8_t  readPin(IO pin);

void    togglePin(IO pin);
void    setDefaultState(IO pin);

uint8_t getDefaultState(IO pin);
uint8_t getPinDir(IO pin);

#endif
