/*
 * chip_it.c
 *
 *  Created on: Jan 7, 2021
 *      Author: AJAY
 */


#include "chip_it.h"
#include "stdint.h"
#include "stm32f1xx.h"
#include "chip.h"
#include "timer.h"

volatile uint32_t systick = 0;


void SysTick_Handler_EX(void)
{
	systick++;
}

uint32_t getSysTick(void)
{
	return (systick);
}


void EXTI9_5_IRQHandler_EX(void)
{
	if(EXTI->PR & GPIO_PIN_6){ systemReset();}
}

void processInterrupt(pUartHandler Uh)
{
	if((Uh->Instance->SR & USART_SR_RXNE) != RESET && ((Uh->Instance->CR1 & USART_CR1_RXNEIE) != RESET))
	{
		__NOP();
		Uh->lastChar = systick;

		Uh->rxBuf[Uh->rx_p] = Uh->Instance->DR;
		Uh->rx_p = (Uh->rx_p + 1);

		if(Uh->rx_p ==  Uh->maxRxBufLen){
			Uh->rx_p = 0;
		}
		return;
	}

	else if((Uh->Instance->SR & USART_SR_TXE) != RESET && ((Uh->Instance->CR1 & USART_CR1_TXEIE) != RESET))
	{
		Uh->Instance->DR = (uint8_t)(Uh->txBuf[Uh->tx_g]& (uint8_t)0x00FF);
		Uh->tx_g  = Uh->tx_g+1;

		if(Uh->tx_g >= Uh->maxTxBufLen)
		{
			Uh->tx_g = 0;
		}

		if(Uh->tx_g == Uh->tx_p)
		{
			Uh->trigger = 1;
			__HAL_UART_DISABLE_IT(Uh, UART_IT_TXE);
		}
	}
}

void ADC1_2_IRQHandler_EX(void)
{

}

pUartHandler UART1;
pUartHandler UART2;

void USART1_SetInterruptCtx(pUartHandler u)
{
	UART1 = u;
}
 void USART1_IRQHandler_EX(void)
{
//	 processInterrupt(&uart1);
	 if(!UART1) return;
	 processInterrupt(UART1);
}

 void USART2_SetInterruptCtx(pUartHandler u)
 {
 	UART2 = u;
 }

void USART2_IRQHandler_EX(void)
{
//	processInterrupt(&uart2);
	if(!UART2) return;
	processInterrupt(UART2);
}
