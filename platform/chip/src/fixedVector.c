/*
 * fixedVector.c
 *
 *  Created on: May 24, 2021
 *      Author: AJAY
 */

/*************************************************************************************************/
/*NOTE:Order of definition should be same as prototype listed below in all future versions*/

void NMI_Handler(void);
void HardFault_Handler(void);
void MemManage_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);
void SVC_Handler(void);
void DebugMon_Handler(void);
void PendSV_Handler(void);
void WWDG_IRQHandler(void);
void PVD_IRQHandler(void);
void TAMPER_IRQHandler(void);
void RTC_IRQHandler(void);
void FLASH_IRQHandler(void);
void RCC_IRQHandler(void);
void EXTI0_IRQHandler(void);
void EXTI1_IRQHandler(void);
void EXTI2_IRQHandler(void);
void EXTI3_IRQHandler(void);
void EXTI4_IRQHandler(void);
void DMA1_Channel1_IRQHandler(void);
void DMA1_Channel2_IRQHandler(void);
void DMA1_Channel3_IRQHandler(void);
void DMA1_Channel4_IRQHandler(void);
void DMA1_Channel5_IRQHandler(void);
void DMA1_Channel6_IRQHandler(void);
void DMA1_Channel7_IRQHandler(void);
void USB_HP_CAN1_TX_IRQHandler(void);
void USB_LP_CAN1_RX0_IRQHandler(void);
void CAN1_RX1_IRQHandler(void);
void CAN1_SCE_IRQHandler(void);
void TIM1_BRK_IRQHandler(void);
void TIM1_UP_IRQHandler(void);
void TIM1_TRG_COM_IRQHandler(void);
void TIM1_CC_IRQHandler(void);
void TIM2_IRQHandler(void);
void TIM3_IRQHandler(void);
void TIM4_IRQHandler(void);
void I2C1_EV_IRQHandler(void);
void I2C1_ER_IRQHandler(void);
void I2C2_EV_IRQHandler(void);
void I2C2_ER_IRQHandler(void);
void SPI1_IRQHandler(void);
void SPI2_IRQHandler(void);
void USART3_IRQHandler(void);
void EXTI15_10_IRQHandler(void);
void RTC_Alarm_IRQHandler(void);
void USBWakeUp_IRQHandler(void);
void SysTick_Handler(void);
void ADC1_2_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
void USART1_IRQHandler(void);
void USART2_IRQHandler(void);
void defaultHandler(void);
/***************************************************************************/

extern void SysTick_Handler_EX   (void);
extern void ADC1_2_IRQHandler_EX (void);
extern void USART1_IRQHandler_EX (void);
extern void USART2_IRQHandler_EX (void);
extern void EXTI9_5_IRQHandler_EX(void);

/***************************************************************************/

void NMI_Handler(void)       	 		{defaultHandler();}
void HardFault_Handler(void)	 		{defaultHandler();}
void MemManage_Handler(void) 	 		{defaultHandler();}
void BusFault_Handler(void) 	        {defaultHandler();}
void UsageFault_Handler(void)	        {defaultHandler();}
void SVC_Handler(void)			        {defaultHandler();}
void DebugMon_Handler(void) 	        {defaultHandler();}
void PendSV_Handler(void) 		        {defaultHandler();}
void WWDG_IRQHandler(void) 		        {defaultHandler();}
void PVD_IRQHandler(void)		        {defaultHandler();}
void TAMPER_IRQHandler(void)	        {defaultHandler();}
void RTC_IRQHandler(void) 		        {defaultHandler();}
void FLASH_IRQHandler(void) 	        {defaultHandler();}
void RCC_IRQHandler(void) 	  	        {defaultHandler();}
void EXTI0_IRQHandler(void)		        {defaultHandler();}
void EXTI1_IRQHandler(void)		        {defaultHandler();}
void EXTI2_IRQHandler(void)		        {defaultHandler();}
void EXTI3_IRQHandler(void)		        {defaultHandler();}
void EXTI4_IRQHandler(void) 	        {defaultHandler();}
void DMA1_Channel1_IRQHandler(void)     {defaultHandler();}
void DMA1_Channel2_IRQHandler(void)     {defaultHandler();}
void DMA1_Channel3_IRQHandler(void)     {defaultHandler();}
void DMA1_Channel4_IRQHandler(void)     {defaultHandler();}
void DMA1_Channel5_IRQHandler(void)     {defaultHandler();}
void DMA1_Channel6_IRQHandler(void)     {defaultHandler();}
void DMA1_Channel7_IRQHandler(void)     {defaultHandler();}
void USB_HP_CAN1_TX_IRQHandler(void)    {defaultHandler();}
void USB_LP_CAN1_RX0_IRQHandler(void)   {defaultHandler();}
void CAN1_RX1_IRQHandler(void)          {defaultHandler();}
void CAN1_SCE_IRQHandler(void)          {defaultHandler();}
void TIM1_BRK_IRQHandler(void)			{defaultHandler();}
void TIM1_UP_IRQHandler(void) 			{defaultHandler();}
void TIM1_TRG_COM_IRQHandler(void)		{defaultHandler();}
void TIM1_CC_IRQHandler(void) 			{defaultHandler();}
void TIM2_IRQHandler(void) 				{defaultHandler();}
void TIM3_IRQHandler(void) 				{defaultHandler();}
void TIM4_IRQHandler(void) 				{defaultHandler();}
void I2C1_EV_IRQHandler(void) 			{defaultHandler();}
void I2C1_ER_IRQHandler(void) 			{defaultHandler();}
void I2C2_EV_IRQHandler(void) 			{defaultHandler();}
void I2C2_ER_IRQHandler(void) 			{defaultHandler();}
void SPI1_IRQHandler(void) 				{defaultHandler();}
void SPI2_IRQHandler(void) 				{defaultHandler();}
void USART3_IRQHandler(void) 			{defaultHandler();}
void EXTI15_10_IRQHandler(void) 		{defaultHandler();}
void RTC_Alarm_IRQHandler(void) 		{defaultHandler();}
void USBWakeUp_IRQHandler(void) 		{defaultHandler();}

void SysTick_Handler(void)   			{ SysTick_Handler_EX();   }

void ADC1_2_IRQHandler(void)  			{ ADC1_2_IRQHandler_EX(); }

void EXTI9_5_IRQHandler(void) 			{ EXTI9_5_IRQHandler_EX();}

void USART1_IRQHandler(void)  			{ USART1_IRQHandler_EX(); }

void USART2_IRQHandler(void)  			{ USART2_IRQHandler_EX(); }

/***************************************************************************/
void defaultHandler(void){for(;;);}
/***************************************************************************/
