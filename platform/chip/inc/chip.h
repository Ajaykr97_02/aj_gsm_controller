/*
 * chip.h
 *
 *  Created on: Jan 5, 2021
 *      Author: AJAY
 */

#ifndef CHIP_INC_CHIP_H_
#define CHIP_INC_CHIP_H_

#include "stdint.h"
#include "stm32f103xb.h"

#define WDRESET ((IWDG_TypeDef *)IWDG)->KR = 0x0000AAAA

typedef union AIRCR_
{
	struct AIRCREG
	{
		uint8_t reserv2:2;
		uint8_t sysresetreq:1;
		uint16_t reserv1:12;
		uint8_t  endianbit:1;
		uint16_t airvector;
	}aircontainer;
	uint32_t vecmap;

}aircreg , *paircreg;


void chipsetup(void);
void watchDogReset(void);
void systemReset(void);

#endif /* CHIP_INC_CHIP_H_ */
