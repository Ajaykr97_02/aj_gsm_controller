/*
 * chip_it.h
 *
 *  Created on: Jan 7, 2021
 *      Author: AJAY
 */

#ifndef CHIP_INC_CHIP_IT_H_
#define CHIP_INC_CHIP_IT_H_

#include "stdint.h"
#include "uart.h"

uint32_t getSysTick(void);

void SysTick_Handler_EX(void);
void EXTI9_5_IRQHandler_EX(void);
void ADC1_2_IRQHandler_EX(void);
void USART1_IRQHandler_EX(void);
void USART2_IRQHandler_EX(void);

void USART1_SetInterruptCtx(pUartHandler u);
void USART2_SetInterruptCtx(pUartHandler u);

#endif /* CHIP_INC_CHIP_IT_H_ */
