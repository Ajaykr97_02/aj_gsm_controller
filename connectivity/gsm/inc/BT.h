/*
 * BT.h
 *
 *  Created on: May 31, 2021
 *      Author: AJAY
 */

#ifndef GSM_INC_BT_H_
#define GSM_INC_BT_H_


#include "console.h"
#include "gsm.h"

#define BTDEBUG BTPRINTF

pGsmBT gsmBTInit(pconsoleHandler btConsoleTunnel , pconsolecommand btCommand);

void BT_PowerOn(pGsmHandler hdl);
void BT_ACPT(pGsmHandler hdl);

cmdCallBackFunc(BT_PowerOn_callback);

void sendSPPData(pGsmHandler hdl , uint8_t *dBuf , uint16_t len);
void parseSPPdata(pGsmHandler hdl , uint8_t *dBuf ,uint16_t len);

void printBThelpstring(pconsolecommand cmd);

void        setDebugBtCtx(pGsmHandler u);
void        BTPRINTF(char *fmt, ...);

#endif /* GSM_INC_BT_H_ */
