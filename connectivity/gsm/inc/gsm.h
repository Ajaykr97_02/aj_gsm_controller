/*
 * gsm.h
 *
 *  Created on: Jan 12, 2021
 *      Author: AJAY
 */

#ifndef INC_GSM_H_
#define INC_GSM_H_

#include "timer.h"
#include "uart.h"
#include "gsmAPI.h"
#include "console.h"

/*select GSM MODULE */
/********************/

#define _AT_TUNNEL


#define TYPE_ACK  1
#define TYPE_NACK 2


#define CMD_NEW       0
#define CMD_CONTINUE  1

#define cmdCallBackFunc(NAME) int NAME(void *args)

typedef int  (*cmdCallBackFuncPtr)(void *);
typedef void (*atInterpretorFuncPtr)(void * ,uint8_t* , uint16_t);
typedef void (*respfuncPtr)(void * ,uint8_t* , uint16_t);


#define ADD_RESP(NAME) { #NAME , NAME ## _Method}

#define ADD_ACKNACK(NAME , TYPE)  { #NAME ,TYPE ,0 ,0}
#define FREESPACE(start , end , bufferSize)  (end>start)?(end-start):(bufferSize -(start -end))

/********************************************************************/
typedef struct gsmResponceCommand_st
{
	char        *command;
	respfuncPtr func;

}gsmResp , *pgsmResp;


typedef struct gsmAckNack_holder
{
	char     *ackNackString;

	uint8_t   type:2;
	uint8_t   ack:1 ;
	uint8_t   nack:1;

}ackNack_hol , *pAckNack_hol;

typedef struct gsmAckNack_handler_st
{
	uint8_t ackPrime:1 ;
	uint8_t nackPrime:1;

	pAckNack_hol  ackNack_hol;

}ackNack_h , *pAckNack_h;

typedef struct cmdBufferHandler_st
{
	uint8_t  *buf;
	uint16_t maxBufLen;
	uint16_t p;
	uint16_t g;

}cmdBufHdl , *pCmdBufHdl;

typedef struct gsmSend_st
{
	pUartHandler 		 utx;

	uint8_t      		 active:1;
	uint8_t      		 live:1;
	uint8_t       		 resp:1;
	uint8_t              onSuccessBit:1;

	pCmdBufHdl    		 cmdBuf;
	pAckNack_h   		 ackNack;
	cmdCallBackFuncPtr   callBack;
	void                 *callBackArgs;

	int16_t       		 bufIndex;
	uint16_t    		 len;
	uint16_t    	     timeout;
	uint8_t      		 retryCount;
	uint32_t      		 startTime;

}gsmSend , *pGsmSend;

typedef struct gsmCommandHandler_st
{
	cmdBufHdl cmdBuf;

	uint16_t  commandCountLoad;
	uint16_t  maxCommand;

	gsmSend   *send;

}gsmCmd_h , *pGsmCmd_h;

typedef struct gsmFlags_st
{
	uint8_t gsmTunnel:1;
	uint8_t callBackStatusFlag:1;

}gsmFlags , *pGsmFlags;

typedef struct gsmNetworkStatusHolder_st
{
	struct registration
	{
		int8_t cpinStatus;
		int8_t regStatus;
		int8_t resultCode;
		int8_t signalQuality;
		int8_t gprsStatus;
		int8_t cipstatus;
	}reg;

	struct networkParam
	{
		uint8_t localIp[4];
		uint8_t currentAPN[32];

	}net;
}gsmNetStat , *pGsmNetStat;

/************************************************************/
#if BT
typedef enum
{
	A2DP_SRC =0,
	HFP_AG,
	AVRCP_TARGET,
	A2DP,
	SPP,
	HFP,
	HSP,
	BT_PROFILE_END
}BT_PROFILE;

extern char *profileString[BT_PROFILE_END];

typedef struct BTConnectionInfo_St
{
	uint8_t     hostId;
	char 		hostName[32];
	char 		hostMac[32];
	BT_PROFILE  profile;

}BTCon , *pBTcon;

typedef struct gsmBTHolder_ST
{
	uint8_t          BTPowerStatus;
	BTCon            connection;
	pconsolecommand  btCMD;
#if BTCONSOLETUNNEL
	pconsoleHandler  consoleCtx;
#endif
}gsmBT , *pGsmBT;

#endif
/************************************************************/

typedef struct gsmHandler_st                                     // main handle
{
    	pUartHandler           uctx;
		uint8_t                *buf;
		uint16_t               index;
		uint16_t               maxBufLen;
		atInterpretorFuncPtr   asciiInterpretor;
		atInterpretorFuncPtr   binaryInterpretor;
		pUartHandler           conTunnel;
		gsmCmd_h               cmdHandeler;
		ackNack_h              ackNack;
		pgsmResp               respHold;
		gsmFlags               flags;
		gsmNetStat             netStat;
#if BT
		pGsmBT				   BTctx;
#endif
}gsmHandler , *pGsmHandler;

/**********************************************************************/

pGsmHandler gsmInit(pUartHandler gsmUart ,uint16_t gsmBufLen ,uint16_t gsmCmdBufLen ,pUartHandler consoleTunnel ,uint16_t cmdCount ,pAckNack_hol ackNack , pgsmResp responce);
int addGsmBTctx(pGsmHandler gsmHdl , pGsmBT btCtx);

void GsmTunnel(pGsmHandler hdl ,uint8_t state);

void gsmProcess(pGsmHandler gsmH);
void atParsetProcess(pGsmHandler gsmH);
int sendGsmCommand( void *args);

int getFreeSendCtxCount(pGsmHandler hdl);
int sendGsm(pGsmHandler hdl , uint8_t *data , uint16_t length  , uint16_t timeout, uint8_t retry, uint8_t continueFlag, cmdCallBackFuncPtr callback);

void gsmEchoOff(pGsmHandler hdl);

#endif /* INC_G_ATPARSER_H_ */
