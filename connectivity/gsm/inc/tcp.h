/*
 * tcp.h
 *
 *  Created on: Jun 17, 2021
 *      Author: AJAY
 */

#ifndef GSM_INC_TCP_H_
#define GSM_INC_TCP_H_

#include "gsm.h"

typedef struct tcpInfoHolder_St
{
	uint8_t     serverIp[4];
	uint16_t    port;
	pGsmHandler handle;

}tcpInfohdl , *pTcpInfohdl;


pTcpInfohdl tcpInit(uint8_t *serverIP , uint16_t port , pGsmHandler gsmHdl);

#endif /* GSM_INC_TCP_H_ */
