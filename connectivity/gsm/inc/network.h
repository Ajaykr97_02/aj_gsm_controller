/*
 * network.h
 *
 *  Created on: Jun 9, 2021
 *      Author: AJAY
 */

#ifndef GSM_INC_NETWORK_H_
#define GSM_INC_NETWORK_H_

#include "gsmAPI.h"
#include "gsm.h"
#include "thread.h"

void startGsmNetCheck(pGsmHandler gsm , uint32_t timer);

void parseCPIN(pGsmHandler hdl , uint8_t *dBuf ,uint16_t len);
void parseCSQ(pGsmHandler hdl , uint8_t *dBuf ,uint16_t len);
void parseCREG(pGsmHandler hdl , uint8_t *dBuf ,uint16_t len);
void parseCGATT(pGsmHandler hdl , uint8_t *dBuf ,uint16_t len);
void parseCIPSTATE(pGsmHandler hdl , uint8_t *dBuf ,uint16_t len);

#endif /* GSM_INC_NETWORK_H_ */
