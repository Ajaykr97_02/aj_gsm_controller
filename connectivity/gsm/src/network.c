/*
 * network.c
 *
 *  Created on: Jun 9, 2021
 *      Author: AJAY
 */


#include "network.h"
#include "config.h"

/*
 at+cpin?
 at+csq?
 at+creg?
 at+cgatt?
 at+cstt= "{apn}"
 at+ciicr
 at+cifsr
 */

THREAD_FUNC(getNetworkStat);

void startGsmNetCheck(pGsmHandler gsm , uint32_t timer)
{
    loadThread(getNetworkStat, gsm , TIMER_THREAD, timer);
}

cmdCallBackFunc(CPINCallback)
{
	pGsmHandler g = (pGsmHandler)args;
	int type = g->flags.callBackStatusFlag;

	if(!type) g->netStat.reg.cpinStatus = -1;

	return(1);
}

cmdCallBackFunc(CSQCallback)
{
	pGsmHandler g = (pGsmHandler)args;
	int type = g->flags.callBackStatusFlag;

	if(!type) g->netStat.reg.signalQuality = -1;

	return(1);
}

cmdCallBackFunc(CREGCallback)
{
	pGsmHandler g = (pGsmHandler)args;
	int type = g->flags.callBackStatusFlag;

	if(!type) g->netStat.reg.regStatus = -1;

	return(1);
}

cmdCallBackFunc(CGATTCallback)
{
	pGsmHandler g = (pGsmHandler)args;
	int type = g->flags.callBackStatusFlag;

	if(!type) g->netStat.reg.gprsStatus = -1;

	return(1);
}

cmdCallBackFunc(CIPSTATUSCallback)
{
	pGsmHandler g = (pGsmHandler)args;
	int type = g->flags.callBackStatusFlag;

	if(!type) g->netStat.reg.cipstatus = -1;

	return(1);
}

THREAD_FUNC(getNetworkStat)
{
	pGsmHandler hdl = (pGsmHandler)args;
	if(!hdl) return(1);
	if(getFreeSendCtxCount(hdl) < 4) return(0);

	sendGsm(hdl, (uint8_t *)"at+cpin?\n"  , 9  , 1500, 1, CMD_NEW, CPINCallback);
	sendGsm(hdl, (uint8_t *)"at+csq\n"    , 7  , 1500, 1, CMD_NEW, CSQCallback);
	sendGsm(hdl, (uint8_t *)"at+creg?\n"  , 9  , 2000, 1, CMD_NEW, CREGCallback);
	sendGsm(hdl, (uint8_t *)"at+cgatt?\n" , 10 , 2000, 1, CMD_NEW, CGATTCallback);

	sendGsm(hdl, (uint8_t *)"at+cipstatus\n"  , 13 , 3000, 1, CMD_NEW, CIPSTATUSCallback);

	return(0);
}

void setAPN(pGsmHandler hdl, uint8_t *apn)
{
	uint8_t buf[128];

	sprintf((char*)buf , "AT+CSTT=\"%s\"\n" , apn);

	sendGsm(hdl, buf , strlen((char*)buf) , 3000, 1, CMD_NEW, 0);
}

void parseCPIN(pGsmHandler hdl , uint8_t *dBuf ,uint16_t len)
{
	if(strncmp((char *)dBuf , "READY" , 5) == 0)
	{
		hdl->netStat.reg.cpinStatus = 1;
	}

	else
	{
		hdl->netStat.reg.cpinStatus = 0;
	}
}

void parseCSQ(pGsmHandler hdl , uint8_t *dBuf ,uint16_t len)
{
	uint8_t buffer[16] , *d;
	int16_t temp;

	d = buffer;

	while(*dBuf != ',') {*d = *dBuf; d++ ; dBuf++;}
	dBuf++;

	sscanf((char*)buffer, "%hhd" ,(char *)&temp);
	hdl->netStat.reg.signalQuality = temp;
}

void parseCREG(pGsmHandler hdl , uint8_t *dBuf ,uint16_t len)
{
	uint8_t buffer[16] , *d;
	int16_t temp;
	d = buffer;

	while(*dBuf != ',') {*d = *dBuf; d++ ; dBuf++;}
	dBuf++;

	sscanf((char*)buffer, "%hhd" ,(char *)&temp);
	hdl->netStat.reg.resultCode = temp;

	sscanf((char*)dBuf  , "%hhd" ,(char *)&temp);
	hdl->netStat.reg.regStatus = temp;
}

void parseCGATT(pGsmHandler hdl , uint8_t *dBuf ,uint16_t len)
{
	int16_t temp;

	sscanf((char*)dBuf, "%hhd" ,(char *)&temp);
	hdl->netStat.reg.gprsStatus = temp;
}



void parseCIPSTATE(pGsmHandler hdl , uint8_t *dBuf ,uint16_t len)
{
	if(strncmp((char *)dBuf , "IP INITIAL" , 10) == 0)
	{
		hdl->netStat.reg.cipstatus = 1;
		setAPN(hdl , globalConfig.gInfo.apn);
	}
	else if(strncmp((char *)dBuf , "IP START" , 8) == 0)
	{
		hdl->netStat.reg.cipstatus = 2;
		sendGsm(hdl, (uint8_t *)"AT+CIICR\n", 9,3000, 1, CMD_NEW, 0);
	}
	else if(strncmp((char *)dBuf , "IP CONFIG" , 9) == 0)
	{
		hdl->netStat.reg.cipstatus = 3;
	}
	else if(strncmp((char *)dBuf , "IP GPRSACT" , 10) == 0)
	{
		hdl->netStat.reg.cipstatus = 4;
		sendGsm(hdl, (uint8_t *)"AT+CIFSR\n", 9,3000, 1, CMD_NEW, 0);
	}
	else if(strncmp((char *)dBuf , "IP STATUS " , 9) == 0)
	{
		hdl->netStat.reg.cipstatus = 5;
	}
	else if(strncmp((char *)dBuf , "TCP CONNECTING" , 14) == 0)
	{
		hdl->netStat.reg.cipstatus = 6;
	}
	else if(strncmp((char *)dBuf , "CONNECT OK" , 10) == 0)
	{
		hdl->netStat.reg.cipstatus = 7;
	}
	else if(strncmp((char *)dBuf , "TCP CLOSING" , 11) == 0)
	{
		hdl->netStat.reg.cipstatus = 8;
	}
	else if(strncmp((char *)dBuf , "TCP CLOSED" , 10) == 0)
	{
		hdl->netStat.reg.cipstatus = 9;
	}
	else if(strncmp((char *)dBuf , "PDP DEACT" , 9) == 0)
	{
		hdl->netStat.reg.cipstatus = 10;
	}
}



