/*
 * gsm.c
 *
 *  Created on: Jan 12, 2021
 *      Author: AJAY
 */

#include "gsm.h"
#include "thread.h"
#include "chip_it.h"
#include "console.h"
#include "indic.h"
#include "BT.h"
#include "stdlib.h"

void gsmCmdInterpretor(void *handle ,uint8_t* dBuf, uint16_t dLen);
void gsmPDUInterpretor(void *handle ,uint8_t* dBuf, uint16_t dLen);
uint8_t gsmAckChecker(pAckNack_h  pAckNack , uint8_t *dBuf , uint16_t len);

int copyBuffer(pCmdBufHdl hdl,  uint8_t *dBufDest , uint16_t index ,uint16_t len);
int clearBuffer(pCmdBufHdl hdl , uint16_t index , uint16_t len);


pGsmHandler gsmInit(pUartHandler gsmUart ,uint16_t gsmBufLen ,uint16_t gsmCmdBufLen ,pUartHandler consoleTunnel ,uint16_t cmdCount ,pAckNack_hol ackNack , pgsmResp responce)
{
	pGsmHandler g = calloc(1 , sizeof(gsmHandler));

	if(!g) return (0);

	g->uctx         = gsmUart;
	g->buf          = calloc(1,gsmBufLen);
	g->maxBufLen    = gsmBufLen;
	g->conTunnel    = consoleTunnel;

	g->cmdHandeler.cmdBuf.buf         = calloc(1, gsmCmdBufLen);
	g->cmdHandeler.cmdBuf.maxBufLen   = gsmCmdBufLen;
	g->cmdHandeler.maxCommand         = cmdCount;
	g->cmdHandeler.send               = calloc(cmdCount , sizeof(gsmSend));


	g->asciiInterpretor               = gsmCmdInterpretor;
	g->binaryInterpretor              = gsmPDUInterpretor;

	g->ackNack.ackNack_hol            = ackNack;

	g->respHold                       = responce;

	return (g);

}

int addGsmBTctx(pGsmHandler gsmHdl , pGsmBT btCtx)
{
#if BT
	gsmHdl->BTctx = btCtx;

	return (1);
#else
	return (0);
#endif
}
void gsmProcess(pGsmHandler gsmH)
{
	atParsetProcess(gsmH);
}

void atParsetProcess(pGsmHandler gsmH)
{
	int ch;
	ch = getCharSerial(gsmH->uctx);

	if(ch == -1) return;

	gsmH->buf[gsmH->index] = ch;
	gsmH->index = (gsmH->index +1) & (gsmH->maxBufLen - 1);

	if(ch == '\n')
	{
		if(gsmH->buf[gsmH->index - 2] == '\r' )
		{
			if(gsmH->buf[0] == '\r' && gsmH->buf[1] == '\n'){ gsmH->index = 0; return;}   // remove empty line
			gsmH->asciiInterpretor((void *)gsmH,gsmH->buf , gsmH->index);
		}
		gsmH->index = 0;
	}

	if(ch == '>' && (gsmH->index == 1))
	{
		gsmAckChecker(&gsmH->ackNack ,(uint8_t *) ">" , 1);
		gsmH->index = 0;
	}

	if(gsmH->flags.gsmTunnel) putCharSerial(gsmH->conTunnel, ch);
}

void GsmTunnel(pGsmHandler hdl ,uint8_t state)
{
	hdl->flags.gsmTunnel = state;
}

void gsmCmdInterpretor(void *handle ,uint8_t* dBuf, uint16_t dLen)
{
	pGsmHandler gsmH = (pGsmHandler)handle;

	if(gsmAckChecker(&gsmH->ackNack ,dBuf,dLen)) return;

	if(*dBuf == '+')
	{
		dBuf++;
		dLen--;
	}

	uint8_t *d = dBuf;
	uint8_t *cmd;

	setpramtoken(dBuf, dLen, ' ');
	cmd = fetchtoken();

	for(pgsmResp iter = gsmH->respHold ; iter->command ; iter++)
	{
		if(strncmp(iter->command , (char *)(cmd) ,strlen((char*)iter->command)) == 0)
		{
			iter->func(gsmH ,&d[strlen((char*)cmd)+1] , (dLen - strlen((char*)cmd))-1);
			return;
		}
	}

	return;
}

void gsmPDUInterpretor(void *handle ,uint8_t* dBuf, uint16_t dLen)
{
	return;
}

uint8_t prevMsgState = 1;

THREAD_FUNC(sendGsmCommand)
{
	uint8_t d[256];
	uint16_t index;

	pGsmSend send = (pGsmSend)args;

	index = send->bufIndex;

	if(send->onSuccessBit && !prevMsgState)
	{
		send->ackNack->ackPrime  =0;
		send->ackNack->nackPrime =0;
		send->live               =0;

		send->active             =0;

		clearBuffer(send->cmdBuf, index ,send->len);

		return (1);
	}

	if(send->active && send->live)
	{
		if(send->ackNack->ackPrime){

			send->ackNack->ackPrime  =0;
			send->ackNack->nackPrime =0;
			send->live               =0;

			send->active             =0;
			prevMsgState             =1;

			clearBuffer(send->cmdBuf, index ,send->len);

			if(send->callBack)
			{
				((pGsmHandler)send->callBackArgs)->flags.callBackStatusFlag = 1;
				loadThread((threadFunc)send->callBack, send->callBackArgs , FREEFLOW_THREAD, NA);
			}

			return (1);
		}

		else if(send->ackNack->nackPrime || ((getSysTick() - send->startTime) >= send->timeout))
		{
			send->ackNack->ackPrime  =0;
			send->ackNack->nackPrime =0;
			send->live               =0;

			if (!send->retryCount)
			{
				debug("\nNo Response purge\n");
				prevMsgState         =0;
				send->active         =0;
				clearBuffer(send->cmdBuf, index ,send->len);

				if(send->callBack)
				{
					((pGsmHandler)send->callBackArgs)->flags.callBackStatusFlag = 0;
					loadThread((threadFunc)send->callBack, send->callBackArgs, FREEFLOW_THREAD, NA);
				}
				return (1);
			}
			else
			{
				send->retryCount--;
				return (0);
			}
		}
		else return (0);
	}

	if(send->len >= send->cmdBuf->maxBufLen) return (1);

	copyBuffer(send->cmdBuf , d , index ,send->len);

	send->startTime = getSysTick();
	send->live = 1;

	putStreamSerial(send->utx, d, send->len);

	return (0);
}


uint8_t gsmAckChecker(pAckNack_h  pAckNack , uint8_t *dBuf , uint16_t len)
{
	pAckNack_h p = pAckNack;

	for(pAckNack_hol h = p->ackNack_hol ; h->ackNackString ; h++)
	{
		if(strncmp(h->ackNackString , (char *)dBuf , strlen(h->ackNackString)) == 0)
		{
			if(h->type == TYPE_ACK)
			{
				h->ack  = 1;
				p->ackPrime  =1;
			}
			else if(h->type == TYPE_NACK) {  h->nack = 1; p->nackPrime = 1;}

			return(1);
		}
	}

	return(0);
}

/*******************************************************************************/
int clearBuffer(pCmdBufHdl hdl , uint16_t index , uint16_t len)
{
	if(hdl->g != index)
	{
		error("ERROR:Command buffer misalignment in clear buffer\n");
		return (-1);
	}

	for(int i = 0 ; i < len ; i++)
	{
		hdl->g++;
		if(hdl->g >= hdl->maxBufLen){ hdl->g = 0;}
	}

	return (1);
}

int copyBuffer(pCmdBufHdl hdl,  uint8_t *dBufDest , uint16_t index ,uint16_t len)
{
	if(hdl->g != index)
	{
		error("ERROR:Command buffer misalignment in copy buffer\n");
		return (-1);
	}

	for(int i =0 , j = hdl->g ; i< len ; i++)
	{
		dBufDest[i] = hdl->buf[j];
		j++;
		if(j >= hdl->maxBufLen) j = 0;
	}

	return (1);
}

int assignBuffer(pGsmCmd_h hdl, uint8_t *dBuf, uint16_t len)
{
	uint16_t cInx = 0;

	int f = FREESPACE(hdl->cmdBuf.p , hdl->cmdBuf.g , hdl->cmdBuf.maxBufLen);

	if( f < len) return (-1);

	cInx = hdl->cmdBuf.p;

	for(int i = 0 ; i<len ;i++)
	{
		hdl->cmdBuf.buf[hdl->cmdBuf.p] = dBuf[i];
		hdl->cmdBuf.p++;
		if(hdl->cmdBuf.p >= hdl->cmdBuf.maxBufLen) hdl->cmdBuf.p = 0;
	}

	return (cInx);
}

int getFreeSendCtxCount(pGsmHandler hdl)
{
	int count = 0;

	for(int i = 0 ; i < hdl->cmdHandeler.maxCommand ; i ++)
	{
		if(!hdl->cmdHandeler.send[i].active) count++;
	}

	return(count);

}
pGsmSend getSendCtx(pGsmHandler hdl)
{
	pGsmSend s;

	if(hdl->cmdHandeler.send[hdl->cmdHandeler.commandCountLoad].active) return((pGsmSend)0);

	s = &hdl->cmdHandeler.send[hdl->cmdHandeler.commandCountLoad];
	hdl->cmdHandeler.commandCountLoad = hdl->cmdHandeler.commandCountLoad + 1;

	if(hdl->cmdHandeler.commandCountLoad >= hdl->cmdHandeler.maxCommand) hdl->cmdHandeler.commandCountLoad = 0;

	return (s);

}

int sendGsm(pGsmHandler hdl , uint8_t *data , uint16_t length  , uint16_t timeout, uint8_t retry, uint8_t continueFlag, cmdCallBackFuncPtr callback)
{
	pGsmSend s = getSendCtx(hdl);
	if(!s) return (-1);

	s->cmdBuf  = &hdl->cmdHandeler.cmdBuf;
	s->ackNack = &hdl->ackNack;

	s->bufIndex = assignBuffer(&hdl->cmdHandeler ,data ,length);
	if(s->bufIndex == -1)
	{
		return (-1);
	}

	s->utx        = hdl->uctx;

	s->live         = 0;
	s->active       = 1;
	s->callBack     = callback;
	s->callBackArgs = hdl;

	s->len          = length;
	s->timeout      = timeout;
	s->retryCount   = (retry-1);        // Zero compensation
	s->onSuccessBit = continueFlag;
	s->startTime    = 0;


	loadThread((threadFunc)sendGsmCommand,  s , ORDERED_THREAD , NA);
	return (1);
}

/****************************************************************/

void gsmEchoOff(pGsmHandler hdl)
{
	sendGsm(hdl, (uint8_t *)"ATE0\n", 5 , 3000, 3, CMD_NEW , 0);
}

/****************************************************************/

