/*
 * BT.c
 *
 *  Created on: May 31, 2021
 *      Author: AJAY
 */
#include "stdlib.h"

#include "gsmAPI.h"
#include "BT.h"
#include "gsm.h"
#include "console.h"

void sendSPPData(pGsmHandler hdl , uint8_t *dBuf , uint16_t len);
void btConsoleCmdInterpretor(pGsmHandler hdl , uint8_t *dBuf , uint16_t len);

char *profileString[BT_PROFILE_END] = {"A2DP(Source)" ,"HFP(AG)" ,"AVRCP(Target)" ,"A2DP" ,"SPP" ,"HFP" ,"HSP"};

pGsmBT gsmBTInit(pconsoleHandler btConsoleTunnel , pconsolecommand btCommand)
{
	pGsmBT B = calloc(1 , sizeof(gsmBT));

	if(!B) return(0);

	B->consoleCtx = btConsoleTunnel;
	B->btCMD      = btCommand;

	return (B);
}

void BT_PowerOn(pGsmHandler hdl)
{
	if(!hdl->BTctx) return;

	sendGsm(hdl, (uint8_t *)"AT+BTPOWER=0\n", 13 , 3000, 1, CMD_NEW, 0);
	sendGsm(hdl, (uint8_t *)"AT+BTPOWER=1\n", 13 , 3000, 1, CMD_NEW, BT_PowerOn_callback);
}

cmdCallBackFunc(BT_PowerOn_callback)
{
	int type = ((pGsmHandler)args)->flags.callBackStatusFlag;

	if(type)  {debug("BT:Powered on\n");}
	else      {debug("BT:ERROR\n");}

	return 1;
}

void BT_ACPT(pGsmHandler hdl)
{
	if(!hdl->BTctx) return;

	sendGsm(hdl, (uint8_t *)"AT+BTACPT=1\n", 12 , 3000, 1, CMD_NEW, 0);
}

void sendSPPData(pGsmHandler hdl , uint8_t *dBuf , uint16_t len)
{
	uint8_t data[256 + 18] = {0};
	int ret =0;

	if(len > 256) return;

	if(getFreeSendCtxCount(hdl) < 2) { debug("SEND CTX NOT SUFFICIENT\n"); return;}

	uint8_t *d = data;
	sprintf((char *)d , "AT+BTSPPSEND=%hd\r" , len);

	d = d+ strlen((char*)d);

	ret = sendGsm(hdl, data, (d - data) , 5000, 1, CMD_NEW, 0);

	if(ret == -1) debug("GSM Ctx Full\n");
	d = data;

	memcpy(d ,dBuf, len );

	d = d + len;
	*d = '\n';
	d++;

	ret = sendGsm(hdl, data, (d - data) , 1000, 1, CMD_CONTINUE, 0);
	if(ret == -1) debug("GSM Ctx Full\n");

}

void parseSPPdata(pGsmHandler hdl , uint8_t *dBuf ,uint16_t len)
{
	if(!hdl->BTctx) return;
	int payloadLen;
	uint8_t *d = dBuf;

	while(*d != ','){ d++;}
	d++;

	sscanf((char *)d, "%d", &payloadLen);

	while(*d != ','){ d++;}
	d++;

	if(*d == '+')
	{
		d++;

		if(*d == '^')
		{
			d++;

			uint8_t buf[65];

			if((len - (d -dBuf)) >=64 ) return;

			memcpy(buf , d , (len - (d -dBuf)));
			buf[(len - (d -dBuf))] = '\n';

			sendGsm(hdl, buf , ((len - (d - dBuf))+1) , 15000, 1, CMD_NEW, 0);
		}
		else
		{
			uint8_t buf[65];

			if((len - (d -dBuf)) >=64 ) return;

			memcpy(buf , d , (len - (d -dBuf)));
			buf[(len - (d -dBuf))] = '\n';

			hdl->BTctx->consoleCtx->asciiInterpretor(hdl->BTctx->consoleCtx ,buf , ((len - (d -dBuf))+1));
		}

		sendSPPData(hdl ,(uint8_t *)"RECV" ,4);
	}

	else
	{
		btConsoleCmdInterpretor(hdl ,d , ((len - (d -dBuf))+1));
	}
}

void btConsoleCmdInterpretor(pGsmHandler hdl , uint8_t *dBuf , uint16_t len)
{
	setpramtoken(dBuf, len, ' ');

	while(1)
	{
		tokens = fetchtoken();

		if(tokens == NULL) break;
		tokencount++;
		for(pconsolecommand iter = hdl->BTctx->btCMD ;  iter->commandstr ; iter++)
		{
			if(strcmp(iter->commandstr , (char*)tokens) == 0)
			{
				iter->func(hdl);
				return;
			}
		}
		error("SYNTAX ERROR\n");
		break;
	}
}

void printBThelpstring(pconsolecommand cmd)
{
	for(pconsolecommand cmditer = &cmd[0]; cmditer->commandstr;cmditer++)
	{
		BTDEBUG("%s %s:%s\n" , cmditer->commandstr,printFormatSpace(cmditer->commandstr,8),cmditer->helpstr);
	}

}
