/*
 * tcp.c
 *
 *  Created on: Jun 17, 2021
 *      Author: AJAY
 */
#include "tcp.h"
#include "stdlib.h"
#include "thread.h"


pTcpInfohdl tcpInit(uint8_t *serverIP , uint16_t port , pGsmHandler gsmHdl)
{
	pTcpInfohdl p = malloc(sizeof(tcpInfohdl));

	p->handle = gsmHdl;
	p->port   = port;
	memcpy(p->serverIp, serverIP ,4);

	return(p);
}

THREAD_FUNC(tcpRoutine)
{
	pTcpInfohdl p = (pTcpInfohdl)args;

	switch(p->handle->netStat.reg.cipstatus)
	{
	case 5:{

		break;
	}

	default:{
		break;
	}
	}
	return(0);
}
